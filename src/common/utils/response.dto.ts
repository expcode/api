export class ResponseDto<T> {
  public data: T;
  public info: {
    page: number;
    size: number;
    count: number;
  };
  static create<T>(data: T, page: number, size: number, count: number | null) {
    const dto = new ResponseDto<T>();
    dto.data = data;
    dto.info = {
      page,
      size,
      count,
    };
    return dto;
  }
}
