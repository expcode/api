import { Module } from '@nestjs/common';
import { DatasetService } from './dataset.service';
import { MongoClient } from 'mongodb';

@Module({
  providers: [
    DatasetService,
    {
      provide: 'MONGO',
      useFactory: async () => {
        const url = 'mongodb://localhost:27017';
        const dbName = 'test';
        return MongoClient.connect(url);
      },
    },
  ],
  exports: [DatasetService],
})
export class DatasetModule {}
