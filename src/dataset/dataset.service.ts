import { Inject, Injectable } from '@nestjs/common';
import { Db, MongoClient } from 'mongodb';

@Injectable()
export class DatasetService {
  constructor(@Inject('MONGO') private readonly client: MongoClient) {}

  useDatabase(dbName: string): Db {
    return this.client.db(dbName);
  }
}
