import { Injectable } from '@nestjs/common';
import { CreateModellerDto } from './dto/create-modeller.dto';
import { UpdateModellerDto } from './dto/update-modeller.dto';
import { MetadataService } from '../metadata/metadata.service';
import { DatasetService } from '../dataset/dataset.service';
import { ObjectId } from 'mongodb';
import { ValidatorService } from '../validator/validator.service';
import { ResponseDto } from '../common/utils/response.dto';
import { Fields } from '../metadata/entities/metadatum.entity';

@Injectable()
export class ModellerService {
  constructor(
    private readonly datasetService: DatasetService,
    private readonly validatorService: ValidatorService,
  ) {}

  async create(entityName: string, createModellerDto: CreateModellerDto) {
    const validData = await this.validatorService.validate<CreateModellerDto>(
      entityName,
      createModellerDto,
    );
    return this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .insertOne(validData);
  }

  async batchCreate(
    entityName: string,
    createModellerDtos: CreateModellerDto[],
  ) {
    const toSave = [];
    for (const createModellerDto of createModellerDtos) {
      const validData = await this.validatorService.validate<CreateModellerDto>(
        entityName,
        createModellerDto,
      );
      toSave.push(validData);
    }
    return this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .insertMany(toSave);
  }

  async findAll(
    entityName: string,
    filter: Fields = {},
    page = 1,
    size = 10,
    needCount = false,
  ) {
    const skips = size * (page - 1);
    await this.validatorService.validateEntityName(entityName);
    await this.validatorService.validateFilter(entityName, filter);
    const response = await this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .find(filter)
      .skip(skips)
      .limit(size)
      .toArray();

    const count = needCount ? await this.count(entityName, filter) : null;
    return ResponseDto.create(response, page, size, count);
  }

  async count(entityName: string, filter: Fields = {}): Promise<number> {
    const count = await this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .countDocuments(filter);
    return count;
  }

  async findOne(entityName: string, id: string) {
    await this.validatorService.validateEntityName(entityName);
    return this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .findOne({ _id: new ObjectId(id) });
  }

  async update(
    entityName: string,
    id: string,
    updateModellerDto: UpdateModellerDto,
  ) {
    const validData = await this.validatorService.validate<CreateModellerDto>(
      entityName,
      updateModellerDto,
    );
    return this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .updateOne({ _id: id }, { $set: validData });
  }

  remove(entityName: string, id: string) {
    return this.datasetService
      .useDatabase('someUser')
      .collection(entityName)
      .deleteOne({ _id: new ObjectId(id) });
  }
}
