import { Test, TestingModule } from '@nestjs/testing';
import { ModellerService } from './modeller.service';
import { v4 } from 'uuid';
import * as randomWord from 'random-words';
import { DatasetModule } from '../dataset/dataset.module';
import { ValidatorModule } from '../validator/validator.module';

describe('ModellerService', () => {
  let service: ModellerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatasetModule, ValidatorModule],
      providers: [ModellerService],
    }).compile();

    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
    }

    service = module.get<ModellerService>(ModellerService);
    for (let i = 0; i < 100000; i++) {
      const toSave = [];
      for (let j = 0; j < 10000; j++) {
        toSave.push({
          id: v4(),
          title: randomWord(),
          inn: getRandomIntInclusive(10000000, 99999999),
        });
      }
      await service.batchCreate('client', toSave);
    }
  }, 10000000);

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
