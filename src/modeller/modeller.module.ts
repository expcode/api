import { Module } from '@nestjs/common';
import { ModellerService } from './modeller.service';
import { ModellerController } from './modeller.controller';
import { DatasetModule } from '../dataset/dataset.module';
import { ValidatorModule } from '../validator/validator.module';

@Module({
  imports: [DatasetModule, ValidatorModule],
  controllers: [ModellerController],
  providers: [ModellerService],
})
export class ModellerModule {}
