import { Test, TestingModule } from '@nestjs/testing';
import { ModellerController } from './modeller.controller';
import { ModellerService } from './modeller.service';

describe('ModellerController', () => {
  let controller: ModellerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ModellerController],
      providers: [ModellerService],
    }).compile();

    controller = module.get<ModellerController>(ModellerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
