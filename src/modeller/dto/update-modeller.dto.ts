import { PartialType } from '@nestjs/mapped-types';
import { CreateModellerDto } from './create-modeller.dto';

export class UpdateModellerDto extends PartialType(CreateModellerDto) {}
