import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseBoolPipe,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { ModellerService } from './modeller.service';
import { CreateModellerDto } from './dto/create-modeller.dto';
import { UpdateModellerDto } from './dto/update-modeller.dto';
import { Fields } from '../metadata/entities/metadatum.entity';

@Controller('modeller')
export class ModellerController {
  constructor(private readonly modellerService: ModellerService) {}

  @Post(':entityName')
  create(
    @Param('entityName') entityName: string,
    @Body() createModellerDto: CreateModellerDto,
  ) {
    return this.modellerService.create(entityName, createModellerDto);
  }

  @Get('/:entityName')
  findAll(
    @Query('page', ParseIntPipe) page: number,
    @Query('size', ParseIntPipe) size: number,
    @Query('needCount', ParseBoolPipe) needCount: boolean,
    @Param('entityName') entityName: string,
    @Body() filter: Fields,
  ) {
    return this.modellerService.findAll(
      entityName,
      filter,
      page,
      size,
      needCount,
    );
  }

  @Get('/:entityName/count')
  public async count(
    @Body() filter: Fields,
    @Param('entityName') entityName: string,
  ) {
    const count = await this.modellerService.count(entityName, filter);
    return { count };
  }

  @Get('/:entityName/:id')
  findOne(@Param('entityName') entityName: string, @Param('id') id: string) {
    return this.modellerService.findOne(entityName, id);
  }

  @Patch('/:entityName/:id')
  update(
    @Param('entityName') entityName: string,
    @Param('id') id: string,
    @Body() updateModellerDto: UpdateModellerDto,
  ) {
    return this.modellerService.update(entityName, id, updateModellerDto);
  }

  @Delete('/:entityName/:id')
  remove(@Param('entityName') entityName: string, @Param('id') id: string) {
    return this.modellerService.remove(entityName, id);
  }
}
