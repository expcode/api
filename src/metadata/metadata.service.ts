import { HttpException, Injectable } from '@nestjs/common';
import { CreateMetadatumDto } from './dto/create-metadatum.dto';
import { UpdateMetadatumDto } from './dto/update-metadatum.dto';
import { DatasetService } from '../dataset/dataset.service';
import { ObjectId } from 'mongodb';
import { Metadatum } from './entities/metadatum.entity';
import { response } from 'express';

@Injectable()
export class MetadataService {
  private readonly metadataDb = 'metadataDb';
  private readonly entity = 'entity';
  private readonly collection = this.datasetService
    .useDatabase(this.metadataDb)
    .collection(this.entity);

  constructor(private readonly datasetService: DatasetService) {}

  create(createMetadatumDto: CreateMetadatumDto) {
    return this.collection.insertOne(createMetadatumDto);
  }

  async findAll() {
    const response = await this.collection
      .find({})
      .map((doc) => {
        return new Metadatum(doc);
      })
      .toArray();
    return Promise.all(response);
  }

  findOne(id: string) {
    return this.collection.findOne({ _id: new ObjectId(id) });
  }

  findByEntity(entityName: string) {
    return this.collection
      .findOne({ entity: entityName })
      .then((doc) => new Metadatum(doc))
      .catch((e) => null);
  }

  async update(id: string, updateMetadatumDto: UpdateMetadatumDto) {
    const dbResponse = await this.collection.findOne<Metadatum>({
      _id: new ObjectId(id),
    });
    const entity = new Metadatum(dbResponse);
    entity.entity = updateMetadatumDto.entity;
    entity.fields = updateMetadatumDto.fields;
    return this.collection.updateOne({ _id: entity._id }, { $set: entity });
  }

  remove(id: string) {
    return this.datasetService
      .useDatabase(this.metadataDb)
      .collection(this.entity)
      .deleteOne({ _id: new ObjectId(id) });
  }
}
