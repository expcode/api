export class CreateMetadatumDto {
  entity: string;
  public fields: { [key: string]: string };
}
