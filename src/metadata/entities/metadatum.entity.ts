import { ObjectId } from 'mongodb';

export type FieldType = 'string' | 'uuid' | 'number';
export type Fields = { [key: string]: string };

export class Metadatum {
  public _id: ObjectId;
  entity: string;
  public fields: Fields;

  constructor(raw: any) {
    this._id = new ObjectId(raw._id);
    this.entity = raw.entity;
    this.fields = raw.fields;
  }
}
