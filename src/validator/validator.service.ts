import { HttpException, Injectable } from '@nestjs/common';
import { MetadataService } from '../metadata/metadata.service';
import { validate } from 'uuid';
import { FieldType } from '../metadata/entities/metadatum.entity';

@Injectable()
export class ValidatorService {
  constructor(private readonly metadataService: MetadataService) {}

  async validate<T>(entityName: string, data: T): Promise<T> {
    const meta = await this.metadataService.findByEntity(entityName);
    if (meta.entity !== entityName) {
      throw new HttpException('wrong entity name', 400);
    }
    const valid: T = {} as T;
    for (const key in meta.fields) {
      this.checkType(meta.fields[key] as FieldType, key, data[key]);
      valid[key] = data[key];
    }
    return valid;
  }

  checkType(type: 'string' | 'uuid' | 'number', key: string, value: any) {
    if (type === 'uuid') {
      if (!validate(value)) {
        throw new HttpException(`${key} must be ${type}`, 400);
      }
    } else if (typeof value !== type) {
      throw new HttpException(`${key} must be ${type}`, 400);
    }
  }

  async validateEntityName(entityName: string) {
    const meta = await this.metadataService.findByEntity(entityName);
    if (!meta || meta.entity !== entityName) {
      throw new HttpException('wrong entity name', 400);
    }
  }

  async validateFilter(entityName: string, filter: object) {
    const meta = await this.metadataService.findByEntity(entityName);
    if (!meta) {
      throw new HttpException('wrong entity name', 400);
    }
    for (const key in filter) {
      if (meta.fields[key]) {
        this.checkType(meta.fields[key], key, filter[key]);
      } else {
        throw new HttpException(
          `wrong filter key ${key} for entity ${entityName}`,
          400,
        );
      }
    }
  }
}
