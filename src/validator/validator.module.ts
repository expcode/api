import { Module } from '@nestjs/common';
import { ValidatorService } from './validator.service';
import { MetadataModule } from '../metadata/metadata.module';

@Module({
  imports: [MetadataModule],
  providers: [ValidatorService],
  exports: [ValidatorService],
})
export class ValidatorModule {}
