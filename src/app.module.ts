import { Module } from '@nestjs/common';
import { MetadataModule } from './metadata/metadata.module';
import { DatasetModule } from './dataset/dataset.module';
import { ModellerModule } from './modeller/modeller.module';
import { ValidatorModule } from './validator/validator.module';

@Module({
  imports: [MetadataModule, DatasetModule, ModellerModule, ValidatorModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
